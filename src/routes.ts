import { FastifyInstance } from "fastify";
import { IdentifyController } from "./contact/contact.controller";

export default async function setupRoutes(app: FastifyInstance) {
  const identifyController = new IdentifyController();

  app.get("/", async (request, reply) => {
    return { message: "Hello BiteSpeed!!!" };
  });

  app.get("/contacts", identifyController.contacts);

  app.post("/identify", identifyController.handleIdentify);
}

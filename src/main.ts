import fastify, { FastifyInstance } from "fastify";
import dotenv from "dotenv";
import setupRoutes from "./routes";
dotenv.config();

const app: FastifyInstance = fastify({ logger: true });
setupRoutes(app);

const start = async () => {
  const PORT = process.env.PORT || 3001;
  try {
    await app.listen({ port: +PORT, host: "0.0.0.0" });
    app.log.info(`Server is running on port ${PORT}`);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

start();

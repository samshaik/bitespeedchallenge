import { ResultSetHeader, Pool, PoolConnection } from "mysql2/promise";
import ContactInterface from "../contact/contact.model";

class DatabaseQueries {
  private pool: Pool | PoolConnection;

  constructor(pool: Pool | PoolConnection) {
    this.pool = pool;
  }

  async getAllContacts() {
    const [result] = await this.pool.query("SELECT * FROM contact");
    return result;
  }

  async getContactsByEmailOrPhone(email?: string, phoneNumber?: number) {
    const [results] = await this.pool.query<ContactInterface[]>(
      `SELECT * FROM contact WHERE email = ? OR phoneNumber = ? ORDER BY id ASC`,
      [email, phoneNumber]
    );
    return results;
  }

  async insertContact(
    email?: string,
    phoneNumber?: number,
    linkPrecedence?: string,
    primaryLinkedID?: number
  ) {
    const [result] = await this.pool.query<ResultSetHeader>(
      `INSERT INTO contact (email, phoneNumber, linkedId, linkPrecedence) VALUES (?, ?, ?, ?)`,
      [email, phoneNumber, primaryLinkedID, linkPrecedence]
    );
    return result.insertId;
  }

  async getContactsByLinkedID(id: number) {
    const [results] = await this.pool.query<ContactInterface[]>(
      `SELECT * FROM contact WHERE linkedId = ? or id = ?  ORDER BY id ASC`,
      [id, id]
    );
    return results;
  }

  async updateContactLinkPrecedence(primaryLinkedID: number, updateId: number) {
    await this.pool.query(
      `UPDATE contact SET linkPrecedence = ?, linkedId = ? WHERE id = ? OR linkedId = ?`,
      ["secondary", primaryLinkedID, updateId, updateId]
    );
  }
}

export default DatabaseQueries;

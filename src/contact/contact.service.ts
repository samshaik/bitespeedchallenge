import { db } from "../config/db";
import ContactInterface from "./contact.model";
import DatabaseQueries from "../db/mysql";
export class ContactService {
  private dbQueries: DatabaseQueries;

  constructor() {
    this.dbQueries = new DatabaseQueries(db);
  }
  async getContacts() {
    const rows = await this.dbQueries.getAllContacts();
    return rows;
  }
  async handleIdentify(body: { email?: string; phoneNumber?: number }) {
    const { email, phoneNumber } = body;

    let emails = new Set<string>();
    let phoneNumbers = new Set<number>();
    let secondaryContactIds: number[] = [];
    let allContacts: ContactInterface[] = [];
    let isBothPrimary: boolean = false;

    try {
      const result = await this.dbQueries.getContactsByEmailOrPhone(
        email,
        phoneNumber
      );

      if (result.length > 0) {
        // if both primary
        if (
          result.length > 1 &&
          result[0].linkPrecedence == "primary" &&
          result[1].linkPrecedence == "primary"
        ) {
          isBothPrimary = true;
          allContacts = result;
        } else {
          let queryID = result[0].linkedId ? result[0].linkedId : result[0].id;
          const contacts = await this.dbQueries.getContactsByLinkedID(queryID);
          allContacts = this.removeDuplicates([...contacts, ...result]);
        }
        let primaryLinkedID = allContacts[0].id;

        for (let i = 0; i < allContacts.length; i++) {
          const contact = allContacts[i];
          emails.add(contact.email);
          phoneNumbers.add(contact.phoneNumber);
          contact.linkedId ? secondaryContactIds.push(contact.id) : null;
        }

        // check for two primary contacts and update
        if (isBothPrimary) {
          let updateId = allContacts[1].id;
          await this.dbQueries.updateContactLinkPrecedence(
            primaryLinkedID,
            updateId
          );
          secondaryContactIds.push(updateId);
        }

        if (
          email &&
          phoneNumber &&
          !(emails.has(email) && phoneNumbers.has(phoneNumber))
        ) {
          const insertId = await this.dbQueries.insertContact(
            email,
            phoneNumber,
            "secondary",
            primaryLinkedID
          );
          emails.add(email);
          phoneNumbers.add(phoneNumber);
          secondaryContactIds.push(insertId);
        }
        const response = {
          status: "success",
          data: {
            contact: {
              primaryContatctId: primaryLinkedID,
              emails: Array.from(emails),
              phoneNumbers: Array.from(phoneNumbers),
              secondaryContactIds: secondaryContactIds,
            },
          },
        };
        return response;
      } else {
        const insertId = await this.dbQueries.insertContact(
          email,
          phoneNumber,
          "primary"
        );
        const response = {
          status: "success",
          data: {
            contact: {
              primaryContatctId: insertId,
              emails: [email],
              phoneNumbers: [phoneNumber],
              secondaryContactIds: [],
            },
          },
        };
        return response;
      }
    } catch (err) {
      console.log(err);
      return {
        status: "error",
        data: {},
        error: err,
      };
    }
  }

  removeDuplicates(array: ContactInterface[]) {
    const uniqueMap = new Map();

    for (const item of array) {
      uniqueMap.set(item.id, item);
    }

    return Array.from(uniqueMap.values());
  }
}

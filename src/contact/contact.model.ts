import { RowDataPacket } from "mysql2";

export default interface ContactInterface extends RowDataPacket {
  email: string;
  phoneNumber: number;
  id: number;
  linkPrecedence: string;
  linkedId: number;
}

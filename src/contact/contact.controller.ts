import { FastifyReply, FastifyRequest } from "fastify";
import { ContactService } from "./contact.service";
const contactService = new ContactService();
export class IdentifyController {
  async contacts(request: FastifyRequest, reply: FastifyReply) {
    const contacts = await contactService.getContacts();
    reply.send(contacts);
  }

  async handleIdentify(request: FastifyRequest, reply: FastifyReply) {
    try {
      const body = request.body as Object;
      const response = await contactService.handleIdentify(body);
      if (response.status == "success") {
        reply.status(200).send(response?.data);
      } else {
        reply.status(500).send(response);
      }
    } catch (error) {
      reply.status(500).send({ error: "Internal Server Error" });
    }
  }
}
